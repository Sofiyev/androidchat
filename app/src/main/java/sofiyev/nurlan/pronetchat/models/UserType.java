package sofiyev.nurlan.pronetchat.models;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public enum UserType {
    USER, USER_FROM
};
