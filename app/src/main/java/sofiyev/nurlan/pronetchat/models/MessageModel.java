package sofiyev.nurlan.pronetchat.models;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class MessageModel {

    private String message;
    private UserType userType;

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    private StatusType status;

    public MessageModel(){
        message = "";
        userType = UserType.USER;
        messageTime = 0;
        id = 0;
    }

    public MessageModel(String message, UserType userType, long messageTime, int id) {
        this.message = message;
        this.userType = userType;
        this.messageTime = messageTime;
        this.id = id;
    }

    private long messageTime;
    private int id;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
