package sofiyev.nurlan.pronetchat.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class ChatModel implements Parcelable{

    int id;
    private String lastMessage;





    public static Creator<ChatModel> getCREATOR() {
        return CREATOR;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    private String image;
    private String desc;
    private String chatStatus;
    private String username;
    private String fisrtName;
    private String lastName;

    public String getFisrtName() {
        return fisrtName;
    }

    public void setFisrtName(String fisrtName) {
        this.fisrtName = fisrtName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    int type;

    public ChatModel() {

    }

    public ChatModel(int id, int type, String firstName, String image, String desc, String surname,
                     String chatStatus, String username, String lastName, String lastMessage) {
        this.id = id;
        this.type = type;
        this.image = image;
        this.desc = desc;
        this.chatStatus = chatStatus;
        this.username = username;
        this.fisrtName = firstName;
        this.lastName = lastName;
        this.lastMessage = lastMessage;

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getChatStatus() {
        return chatStatus;
    }

    public void setChatStatus(String chatStatus) {
        this.chatStatus = chatStatus;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }




    protected ChatModel(Parcel in) {
        id = in.readInt();
        type = in.readInt();
        image = in.readString();
        desc = in.readString();
        chatStatus = in.readString();
        username = in.readString();
        fisrtName = in.readString();
        lastName = in.readString();
        lastMessage = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(type);
        dest.writeString(image);
        dest.writeString(desc);
        dest.writeString(chatStatus);
        dest.writeString(username);
        dest.writeString(fisrtName);
        dest.writeString(lastName);
        dest.writeString(lastMessage);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ChatModel> CREATOR = new Parcelable.Creator<ChatModel>() {
        @Override
        public ChatModel createFromParcel(Parcel in) {
            return new ChatModel(in);
        }

        @Override
        public ChatModel[] newArray(int size) {
            return new ChatModel[size];
        }
    };
}
