package sofiyev.nurlan.pronetchat.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class UserModel implements Parcelable{


    public static UserModel getUserModelFromChatModel(ChatModel chatModel){
        UserModel userModel = new UserModel();
        userModel.setId(chatModel.getId());
        userModel.setUsername( chatModel.getUsername() );
        userModel.setFirstName( chatModel.getFisrtName() );
        userModel.setLastName( chatModel.getLastName() );
        return userModel;
    }

    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String lastName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public UserModel(int id, String username){
        this.id = id;
        this.username = username;
    }

    public UserModel() {

    }

    protected UserModel(Parcel in) {
        id = in.readInt();
        username = in.readString();
        firstName = in.readString();
        lastName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(username);
        dest.writeString(firstName);
        dest.writeString(lastName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
