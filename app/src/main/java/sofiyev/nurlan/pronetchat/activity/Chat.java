package sofiyev.nurlan.pronetchat.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.adapter.ChatAdapter;
import sofiyev.nurlan.pronetchat.handler.CreateNewThread;
import sofiyev.nurlan.pronetchat.handler.GetNewMessages;
import sofiyev.nurlan.pronetchat.handler.GetPhotoHandler2;
import sofiyev.nurlan.pronetchat.handler.MessagesHandler;
import sofiyev.nurlan.pronetchat.handler.SendNewMessage;
import sofiyev.nurlan.pronetchat.listener.NewMessagesListener;
import sofiyev.nurlan.pronetchat.listener.RepeatListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.models.MessageModel;
import sofiyev.nurlan.pronetchat.models.StatusType;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.models.UserType;
import sofiyev.nurlan.pronetchat.widget.CircleImageView;

import static sofiyev.nurlan.pronetchat.Utils.Constants.INTERVAL;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class Chat extends AppCompatActivity implements RepeatListener, NewMessagesListener {

    private EmojiconEditText t_current;
    private EmojiconTextView textView;
    private ImageView emojiImageView;
    private ImageView submitButton;
    private View rootView;
    private EmojIconActions emojIcon;
    private CircleImageView profileImage;

    private SessionManager sessionManager;
    private List<MessageModel> messages;
    private ListView listView;
    private AtomicInteger lastId;
    private ChatAdapter chatAdapter;
    private Handler handler = new Handler();
    private UserModel userModel;
    private TextView t_username;

    Runnable runnable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Log.e("melumat", "yaratdi");
        profileImage = (CircleImageView) findViewById(R.id.profileImage);
        final Chat chat = this;
        runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(runnable, INTERVAL / 2);
                getNewMessages(false, 0, chat );
            }
        };

        rootView = findViewById(R.id.activity_chat);
        t_username = (TextView) findViewById(R.id.t_userName);
        emojiImageView = (ImageView) findViewById(R.id.emoji_btn);

        t_current = (EmojiconEditText) findViewById(R.id.currentMessage);
        emojIcon = new EmojIconActions(this, rootView, t_current, emojiImageView);
        emojIcon.ShowEmojIcon();
        emojIcon.setIconsIds(R.drawable.ic_action_keyboard, R.drawable.smiley);
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("melumat", "Keyboard opened!");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("melumat", "Keyboard closed");
            }
        });

//                emojIcon.setUseSystemEmoji(b);
  //              textView.setUseSystemDefault(b);



        userModel = (UserModel) getIntent().getExtras().get("object");
        t_username.setText( userModel.getFirstName() + " " + userModel.getLastName());
        messages = new ArrayList<>();
        sessionManager = SessionManager.getInstance(getApplicationContext());
        listView = (ListView) findViewById(R.id.listView);
        lastId = new AtomicInteger(0); lastId.set(0);

        chatAdapter = new ChatAdapter(Chat.this, messages);
        listView.setAdapter(chatAdapter);
        GetPhotoHandler2.load(getApplicationContext(),Chat.this,profileImage,null,userModel.getUsername());
        MessagesHandler.loadMessages(Chat.this,getApplicationContext(),messages,
                listView,lastId,chatAdapter,this,  userModel );
        handler.post(runnable);

    }


    @Override
    public void startRepeat() {
        //handler.post(runnable);
    }

    private void sendMessage(String messageText, UserType userType) {
        if (messageText.trim().length() == 0)
            return;

        MessageModel message = new MessageModel();
        message.setStatus(StatusType.SENDING);
        message.setUserType(userType);
        message.setMessage(messageText);
        message.setMessageTime(new Date().getTime());
        messages.add(message);

        if (chatAdapter != null) {
            chatAdapter.notifyDataSetChanged();
        }

       // Log.e("chatMessages", chatMessages.size() + "/" + dialogModel.getItem().getMessageThreadId() + "");
        if (userModel.getId() != 0)
            sendMessage(messageText, messages.size() ,this );
        else
            creatMessage(messageText, messages.size(), Chat.this  );

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if( handler != null ){
            handler.removeCallbacks(runnable);
        }

    }

    public void creatMessage(final String message, final int position, final Chat chat) {
        Log.e("melumat","teze yatardi");
        handler.post(new Runnable() {
            @Override
            public void run() {
                CreateNewThread.send(getApplicationContext(),messages,listView,chatAdapter,
                        chat,userModel,message,position);
            }
        });
    }

    public void sendMessage(final String message, final int position, final Chat chat){

        handler.post(new Runnable() {
            @Override
            public void run() {
                SendNewMessage.send(getApplicationContext(),messages,listView,chatAdapter,
                        userModel.getId(),message,position,chat, chat);
            }
        });
    }

    @Override
    public void getNewMessages(final boolean alfa, final int positions, final Chat chat) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                //MessagesHandler.loadMessages(Chat.this,getApplicationContext(),messages,
                //        listView,lastIchatAdapter,chat,  userModel );

                GetNewMessages.getMessages(getApplicationContext(),messages,listView,chatAdapter,chat,
                        userModel, lastId, alfa, positions
                );


            }
        });
    }

    public void sendMessage(View view) {
        Log.e("melumat budu !",t_current.getText().toString() );
        handler.post(new Runnable() {
            @Override
            public void run() {
                sendMessage(t_current.getText().toString(), UserType.USER);
                t_current.getText().clear();
                listView.setSelection(listView.getAdapter().getCount()-1);
            }
        });
    }

    public void backButtonClicked(View view) {
        finish();
    }

}

