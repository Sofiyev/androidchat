package sofiyev.nurlan.pronetchat.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Util;
import sofiyev.nurlan.pronetchat.handler.LoginHandler;
import sofiyev.nurlan.pronetchat.listener.FinishListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class Login extends AppCompatActivity implements FinishListener {

    public SessionManager sessionManager;
    private EditText t_username;
    private EditText t_password;

    private MultiStateView multiStateView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = SessionManager.getInstance(getApplicationContext());



        t_username = (EditText) findViewById(R.id.t_username);
        t_password = (EditText) findViewById(R.id.t_password);
        multiStateView = (MultiStateView) findViewById(R.id.multiStateView);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sessionManager.logedIn()){
            Intent intent = new Intent(getApplicationContext(),Main.class);
            startActivity(intent);
            finish();
        }
    }

    public void submitLogin(View view) {
        String username = t_username.getText().toString().trim();
        String password = t_password.getText().toString().trim();
        if( is_ok(username,password) ){
            if(Util.hasConnection ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE) )   ){
                LoginHandler.startLoginTask(username,password,getApplicationContext(),sessionManager,multiStateView, this);
            }else{
                Toast.makeText(this, R.string.no_access_internet, Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, R.string.fill_username_password, Toast.LENGTH_LONG).show();

        }
    }



    private boolean is_ok(final String username, final String password) {
        if( username == null ||  username.trim().length() == 0 ||
                password == null || password.trim().length() == 0 ) {
            return false;
        }
        return true;
    }


    @Override
    public void onFinish() {
        this.finish();
        SessionManager.close = false;
    }

    public void register(View view) {
        Intent intent = new Intent(getApplicationContext(),Register.class);
        startActivity(intent);
        //finish();
    }

    @Override
    public void finish() {
        super.finish();
    }

}
