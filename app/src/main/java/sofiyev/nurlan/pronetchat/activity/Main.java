package sofiyev.nurlan.pronetchat.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;


import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.adapter.ViewPagerAdapter;
import sofiyev.nurlan.pronetchat.fragment.Users;
import sofiyev.nurlan.pronetchat.fragment.Messages;
import sofiyev.nurlan.pronetchat.fragment.Profile;
import sofiyev.nurlan.pronetchat.manager.SessionManager;


public class Main extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.account,
            R.drawable.chat,
            R.drawable.users
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        Constants.context = getApplicationContext();
        Constants.activity = Main.this;
        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Profile(), "Profile");
        adapter.addFrag(new Messages(), "Messages");
        adapter.addFrag(new Users(), "Users");
        viewPager.setAdapter(adapter);
    }


    public void logout(View view) {
        SessionManager sessionManager = SessionManager.getInstance(getApplicationContext());
        sessionManager.logOut();
        Intent intent = new Intent(getApplicationContext(),Login.class);
        startActivity(intent);
        finish();
    }
}