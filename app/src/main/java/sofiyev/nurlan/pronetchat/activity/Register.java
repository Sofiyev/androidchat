package sofiyev.nurlan.pronetchat.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Util;
import sofiyev.nurlan.pronetchat.handler.RegisterHandler;
import sofiyev.nurlan.pronetchat.listener.FinishListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class Register extends AppCompatActivity implements FinishListener {


    private EditText t_firstName;
    private EditText t_lastName;
    private EditText t_username;
    private EditText t_password;
    private EditText t_password2;
    private EditText t_email;
    private SessionManager sessionManager;
    private MultiStateView multiStateView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        multiStateView = (MultiStateView) findViewById(R.id.multiStateView);
        t_firstName = (EditText) findViewById(R.id.t_firstName);
        t_lastName = (EditText)findViewById(R.id.t_lastName);
        t_username = (EditText)findViewById(R.id.t_username);
        t_password = (EditText)findViewById(R.id.t_password);
        t_password2 = (EditText)findViewById(R.id.t_password2);
        t_email = (EditText)findViewById(R.id.t_email);
        sessionManager = SessionManager.getInstance(getApplicationContext());

    }



    public void submitRegister(View view) {
        String firstName = t_firstName.getText().toString().trim();
        String lastName = t_lastName.getText().toString().trim();
        String username = t_username.getText().toString().trim();
        String password = t_password.getText().toString().trim();
        String password2 = t_password2.getText().toString().trim();
        String email = t_email.getText().toString().trim();

        if( is_ok(firstName,lastName,username,password,password2,email) ){
            if(Util.hasConnection ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE) )   ){
                RegisterHandler.startRegisterTask(username,lastName,firstName,password,email,
                        getApplicationContext(),sessionManager,multiStateView,this);
            }else{

            }
        }

    }

    private void showToast(int i){
        Toast.makeText(this, i , Toast.LENGTH_LONG).show();
    }

    private boolean is_ok(String firstName, String lastName, String username,
                          String password, String password2, String email) {
        if( firstName.length() == 0 ){
            showToast(R.string.fill_firstName);
            return false;
        }else if( lastName.length() == 0 ){
            showToast(R.string.fill_lastName);
            return false;
        }else if( username.length() == 0 ){
            showToast(R.string.fill_username);
            return false;
        }else if( password.length() == 0  ){
            showToast(R.string.fill_password);
            return false;
        }else if( password2.length() == 0  ){
            showToast(R.string.fill_password2);
            return false;
        }else if( email.length() == 0 ){
            showToast(R.string.fill_email);
            return false;
        }else if( !password.equals( password2) ){
            showToast(R.string.fill_password3);
            return false;
        }else if( password.length() < 6  ){
            showToast(R.string.fill_password4);
            return false;
        }else if( Util.hasWhiteSpace(username) ){
            showToast(R.string.fill_username2);
            return false;
        }

        return true;
    }


    @Override
    public void onFinish() {
        finish();
    }

    public void backButtonClicked(View view) {
        finish();
    }
}
