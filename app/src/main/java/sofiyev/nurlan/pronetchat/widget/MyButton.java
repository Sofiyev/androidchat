package sofiyev.nurlan.pronetchat.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */
public class MyButton extends Button {

    private Typeface typeface;

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir.ttf");
        this.setTypeface(typeface);
    }

    public MyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(typeface);
    }

    public MyButton(Context context) {
        super(context);
        this.setTypeface(typeface);
    }

}