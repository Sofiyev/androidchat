package sofiyev.nurlan.pronetchat.listener;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

public interface GetPhotoListener {
    void loadPhoto();
}
