package sofiyev.nurlan.pronetchat.listener;

import sofiyev.nurlan.pronetchat.activity.Chat;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public interface NewMessagesListener {
    void getNewMessages(final boolean alfa, final int positions, final Chat chat);
}
