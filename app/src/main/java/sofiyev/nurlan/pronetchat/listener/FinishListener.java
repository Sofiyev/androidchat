package sofiyev.nurlan.pronetchat.listener;

/**
 * Created by NurlanSofiyev on 21.09.2017.
 */

public interface FinishListener{
    void onFinish();
}