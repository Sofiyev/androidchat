package sofiyev.nurlan.pronetchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.activity.Main;
import sofiyev.nurlan.pronetchat.handler.GetPhotoHandler;
import sofiyev.nurlan.pronetchat.handler.GetPhotoHandler2;
import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.widget.CircleImageView;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class MessageListAdapter extends BaseAdapter {

    List<ChatModel> list;
    private static LayoutInflater inflater = null;

    public MessageListAdapter(Context context, List<ChatModel> list) {
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.view_message, parent, false);
        CircleImageView imageView = (CircleImageView) vi.findViewById(R.id.profileImage);

        GetPhotoHandler2.load(Constants.context, Constants.activity, imageView,this,list.get(position).getUsername() );

        TextView fullname = (TextView) vi.findViewById(R.id.fullName);
        fullname.setText(list.get(position).getFisrtName() + "  " + list.get(position).getLastName() );
        TextView lastMessage = (TextView) vi.findViewById(R.id.lastMessage);
        lastMessage.setText( list.get(position).getLastMessage() );
        return vi;
    }
    
}
