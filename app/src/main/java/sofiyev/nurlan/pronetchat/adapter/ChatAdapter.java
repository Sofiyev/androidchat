package sofiyev.nurlan.pronetchat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.models.MessageModel;
import sofiyev.nurlan.pronetchat.models.StatusType;
import sofiyev.nurlan.pronetchat.models.UserType;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class ChatAdapter extends BaseAdapter {

    private final List<MessageModel> chatMessages;
    private Activity context;
    AdapterClickListener listener;

    public ChatAdapter(Activity context, List<MessageModel> chatMessages) {
        this.context = context;
        this.chatMessages = chatMessages;
    }

    @Override
    public int getCount() {
        if (chatMessages != null) {
            return chatMessages.size();
        } else {
            return 0;
        }
    }

    @Override
    public MessageModel getItem(int position) {
        return chatMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageModel message = chatMessages.get(position);
        return textMessage(convertView, R.layout.single_message, message);

    }

    private View textMessage(View convertView, int layout, MessageModel message) {
        View v;
        ImageHolder userText;

        if (convertView == null) {
            v = LayoutInflater.from(context).inflate(layout, null, false);
            userText = new ImageHolder();


            userText.singleMessageContainer = (RelativeLayout) v.findViewById(R.id.singleMessageContainer);
            userText.messageTextView = (EmojiconTextView) v.findViewById(R.id.singleMessage);
            userText.imageLeft = (ImageView) v.findViewById(R.id.imageLeft);
            userText.imageRight = (ImageView) v.findViewById(R.id.imageRight);

            v.setTag(userText);
        } else {
            v = convertView;
            userText = (ImageHolder) v.getTag();

        }

        userText.messageTextView.setText(message.getMessage());

        if (message.getStatus() == StatusType.SENDING){
            userText.singleMessageContainer.setAlpha(0.7f);
        }
        else{
            userText.singleMessageContainer.setAlpha(1f);
        }



        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) userText.messageTextView.getLayoutParams();

        if (message.getUserType() != UserType.USER ){

            params.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        }else {
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }
        userText.messageTextView.setBackgroundResource(message.getUserType() != UserType.USER ? R.drawable.background_green_chat : R.drawable.background_grey_chat);
        if (message.getUserType() != UserType.USER) {
            userText.imageLeft.setVisibility(View.VISIBLE);
            userText.imageRight.setVisibility(View.GONE);
        }else {
            userText.imageRight.setVisibility(View.VISIBLE);
            userText.imageLeft.setVisibility(View.GONE);

        }
        return v;
    }



    private class ImageHolder {

        EmojiconTextView messageTextView;
        ImageView imageLeft;
        ImageView imageRight;
        TextView descTextView;
        TextView acceptTextView;
        TextView rejectTextView;
        RelativeLayout singleMessageContainer;
    }

    public interface AdapterClickListener {
        void clickChanged(int status);

    }

    public void setClickedItem(AdapterClickListener eventListener) {
        listener = eventListener;
    }

}
