package sofiyev.nurlan.pronetchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.handler.GetPhotoHandler2;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.widget.CircleImageView;

/**
 * Created by NurlanSofiyev on 22.09.2017.
 */

public class UserListAdapter extends BaseAdapter {

    List<UserModel> list;
    private static LayoutInflater inflater = null;

    public UserListAdapter(Context context, List<UserModel> list) {
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.view_user, parent, false);

        RelativeLayout background = (RelativeLayout) vi.findViewById(R.id.background);
        TextView fullname = (TextView) vi.findViewById(R.id.fullName);
        CircleImageView circleImageView = (CircleImageView) vi.findViewById(R.id.profileImage);

        GetPhotoHandler2.load(Constants.context, Constants.activity, circleImageView,this,list.get(position).getUsername() );

        fullname.setText(list.get(position).getUsername() );

        return vi;
    }

}
