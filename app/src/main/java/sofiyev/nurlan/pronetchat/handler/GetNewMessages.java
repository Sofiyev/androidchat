package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.EmojiMapUtil;
import sofiyev.nurlan.pronetchat.adapter.ChatAdapter;
import sofiyev.nurlan.pronetchat.listener.NewMessagesListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;

import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.models.MessageModel;
import sofiyev.nurlan.pronetchat.models.StatusType;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.models.UserType;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class GetNewMessages extends AsyncHttpResponseHandler {

    private List<MessageModel> messages;
    private ListView listView;
    private ChatAdapter chatAdapter;
    private int position;
    private NewMessagesListener listener;
    private AtomicInteger lastId;
    private boolean alfa;


    public static void getMessages(Context context, List<MessageModel> messages, ListView listView, ChatAdapter chatAdapter,
                            NewMessagesListener listener, UserModel userModel,
                                   AtomicInteger lastId, boolean alfa, final int i){

        GetNewMessages getNewMessages = new GetNewMessages();
        getNewMessages.messages = messages;
        getNewMessages.listView = listView;
        getNewMessages.chatAdapter = chatAdapter;
        getNewMessages.position = i;
        getNewMessages.listener = listener;
        getNewMessages.lastId = lastId;
        getNewMessages.alfa = alfa;


        SessionManager sessionManager = SessionManager.getInstance(context);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;


        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));
            nameValuePairs.add(new BasicNameValuePair("lastId", lastId.intValue() + ""));
            nameValuePairs.add(new BasicNameValuePair("messageThreadId", userModel.getId() + ""));

            Log.e("params", nameValuePairs.toString());
            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
            Log.e("entitiy", entity + "");
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        client.post(context,Constants.get_NewMessage, entity, "text/plain", getNewMessages );


    }


    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));
            if (jsonObject.getBoolean("success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");


                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    MessageModel message = new MessageModel();
                    message.setMessage( EmojiMapUtil.replaceCheatSheetEmojis( object.getString("text")  ) );


                    message.setId(object.getInt("id"));
                    message.setStatus(StatusType.SEND);
                    if (jsonObject.getString("user").toLowerCase().equals(object.getJSONObject("user").getString("username").toLowerCase()))
                        message.setUserType(UserType.USER);
                    else
                        message.setUserType(UserType.USER_FROM);

                    lastId.set(  object.getInt("id") );

                    if (alfa)
                        messages.remove(position - 1);
                    messages.add(message);

                }

                chatAdapter.notifyDataSetChanged();

//                                chatListView.post(new Runnable() {
//                                    public void run() {
//                                        chatListView.setSelection(chatListView.getCount() - 1);
//                                    }
//                                });
            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

    }
}
