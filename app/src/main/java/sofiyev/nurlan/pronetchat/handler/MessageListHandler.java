package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.EmojiMapUtil;
import sofiyev.nurlan.pronetchat.activity.Chat;
import sofiyev.nurlan.pronetchat.adapter.MessageListAdapter;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class MessageListHandler extends AsyncHttpResponseHandler {

    private List<ChatModel> list;
    private ListView listView;
    private Context context;
    private SessionManager sessionManager;
    private MessageListAdapter adapter;
    private MultiStateView multiStateView;
    private TryAgainListener listener;


    public static void loadMessageList(Context context, List<ChatModel> list, ListView listView,
                                       MessageListAdapter adapter, MultiStateView multiStateView,
                                       TryAgainListener listener){

        MessageListHandler handler = new MessageListHandler();

        handler.list = list;
        handler.listener = listener;
        handler.listView = listView;
        handler.context = context;
        SessionManager sessionManager = SessionManager.getInstance(context);

        handler.sessionManager = sessionManager;
        handler.adapter = adapter;
        handler.multiStateView = multiStateView;

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;
        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));
            nameValuePairs.add(new BasicNameValuePair("start", ""));
            nameValuePairs.add(new BasicNameValuePair("limit", ""));
            nameValuePairs.add(new BasicNameValuePair("unread", "false"));
            nameValuePairs.add(new BasicNameValuePair("countUnread", "false"));

            Log.e("melumat", "duz gelib ! ");
            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
          //  Log.e("entitiy", entity + "");

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        client.post(context, Constants.last_MessageOfThreads , entity, "text/plain", handler);

    }

    @Override
    public void onStart() {
        super.onStart();
       // multiStateView.setViewState(MultiStateView.ViewState.LOADING);
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {

            Log.e("melumat", "duz gelib 2! ");
            JSONObject jsonObject = new JSONObject(new String(responseBody));
            if( jsonObject.getBoolean("success") ){
                String globalName = jsonObject.getString("user");
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                list.clear();

                Log.e("melumat", "uzunlugu " + jsonArray.length());
                for(int i = 0 ; i < jsonArray.length(); ++i){
                    JSONObject object = jsonArray.getJSONObject(i);
                    JSONObject message = object.getJSONObject("message");
                    ChatModel model = new ChatModel();
                    JSONObject user =null;
                    if (message.getJSONObject("messageThread").getJSONObject("from").getString("username").toLowerCase().equals(globalName.toLowerCase())) {
                        user = message.getJSONObject("messageThread").getJSONObject("to");
                        model.setUsername(  user.getString("username") );
                    } else {
                        user=message.getJSONObject("messageThread").getJSONObject("from");
                        model.setUsername(  user.getString("username") );
                    }

                    model.setLastMessage(  EmojiMapUtil.replaceCheatSheetEmojis( message.getString("text")  ) );
                    model.setId(message.getJSONObject("messageThread").getInt("id"));
                    model.setFisrtName(user.getString("firstname"));
                    model.setLastName(user.getString("lastname"));

                    list.add(model);

                }

                adapter.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        Intent intent = new Intent(context, Chat.class);
                                        intent.putExtra("object", UserModel.getUserModelFromChatModel( list.get(position) ) );
                                        context.startActivity(intent);
                                    }
                                });
                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);

                if (list.size() == 0) {
                    multiStateView.setViewState(MultiStateView.ViewState.EMPTY);
                    TextView textView = (TextView) multiStateView.getView(MultiStateView.ViewState.EMPTY).findViewById(R.id.txt1);
                    textView.setText(R.string.no_data_messages);

                }

            }
            else{
                Log.e("melumat","MessagesList sehv");
                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            }
        }catch (JSONException e){
            Log.e("melumat","MessagesList exception");
            e.printStackTrace();
            multiStateView.setViewState(MultiStateView.ViewState.ERROR);
            multiStateView.getView(MultiStateView.ViewState.ERROR).findViewById(R.id.retry)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.tryAgain();
                            //MessageListHandler.loadMessageList(context,list,listView,adapter,multiStateView);
                        }
                    });
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        multiStateView.setViewState(MultiStateView.ViewState.ERROR);
        multiStateView.getView(MultiStateView.ViewState.ERROR).findViewById(R.id.retry)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.tryAgain();
                    }
                });
    }

}
