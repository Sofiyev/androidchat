package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.activity.Main;
import sofiyev.nurlan.pronetchat.listener.FinishListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class LoginHandler extends AsyncHttpResponseHandler {

    private String username;
    private String password;
    private Context context;
    private MultiStateView multiStateView;
    private SessionManager sessionManager;
    private FinishListener finishListener;


    public static void startLoginTask(String username, String password, Context context,
                                      SessionManager sessionManager, MultiStateView multiStateView,
                                      FinishListener finishListener){
        LoginHandler handler = new LoginHandler();

        handler.username = username;
        handler.password = password;
        handler.context = context;
        handler.sessionManager = sessionManager;
        handler.multiStateView = multiStateView;
        handler.finishListener = finishListener;

        AsyncHttpClient client =  new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("username", username));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
        }catch (Exception e){
            e.printStackTrace();
        }

        client.post(context, Constants.login_URL, entity, "text/plain", handler);

    }

    @Override
    public void onStart() {
        super.onStart();
        multiStateView.setViewState(MultiStateView.ViewState.LOADING);
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));
            if (jsonObject.getBoolean("success")) {

                sessionManager.login(jsonObject.getString("token"), username, password);

                Intent intent = new Intent(context, Main.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                context.startActivity(intent);

                finishListener.onFinish();


                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            } else {

                Toast.makeText(context, jsonObject.getString("msg") + "", Toast.LENGTH_SHORT).show();

            }
            multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
        } catch (JSONException e) {
            e.printStackTrace();
            multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
    }



}
