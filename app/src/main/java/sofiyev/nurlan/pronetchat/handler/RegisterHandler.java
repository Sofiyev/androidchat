package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.Util;
import sofiyev.nurlan.pronetchat.activity.Login;
import sofiyev.nurlan.pronetchat.listener.FinishListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class RegisterHandler extends AsyncHttpResponseHandler {

    private Context context;
    private SessionManager sessionManager;
    private FinishListener finishListener;
    private MultiStateView multiStateView;

    public static void startRegisterTask(String username, String lastname, String firstname,
                                         String password, String email,
                                         Context context, SessionManager sessionManager,
                                         MultiStateView multiStateView, FinishListener listener){
        RegisterHandler handler = new RegisterHandler();
        handler.finishListener = listener;
        handler.context = context;
        handler.sessionManager = sessionManager;
        handler.multiStateView = multiStateView;
        AsyncHttpClient client =  new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;

        try {

            JSONObject object = new JSONObject();
            object.put("username", username);
            object.put("lastname", lastname);
            object.put("firstname", firstname);
            object.put("password", password);
            object.put("phone", "");
            object.put("email", email);

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("user", object.toString()));

            //Log.e("params", nameValuePairs.toString());
            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
            //Log.e("entitiy", entity + "");

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        client.post(context, Constants.register_URL, entity, "text/plain", handler );

    }

    @Override
    public void onStart() {
        super.onStart();

        multiStateView.setViewState(MultiStateView.ViewState.LOADING);
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));
            if (jsonObject.getBoolean("success")) {
                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
                sessionManager.signUp(jsonObject.getString("username"), jsonObject.getString("email"));
                //Intent intent = new Intent(context, Login.class);
                //intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                //context.startActivity(intent);
                finishListener.onFinish();
                //Toast.makeText(context, "Giriş əldə etmək üçün mail təsdiqini edin", Toast.LENGTH_SHORT).show();
            } else {
                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);

                JSONObject jsonError = jsonObject.getJSONObject("formErrors");
                List<String> keyList = new ArrayList<>();
                Iterator iterator = jsonError.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    keyList.add(key);

                    Toast.makeText(context, jsonError.getString(key) + "", Toast.LENGTH_SHORT).show();

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
    }



}
