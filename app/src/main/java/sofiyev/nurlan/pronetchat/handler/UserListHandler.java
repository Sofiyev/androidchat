package sofiyev.nurlan.pronetchat.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.activity.Chat;
import sofiyev.nurlan.pronetchat.adapter.MessageListAdapter;
import sofiyev.nurlan.pronetchat.adapter.UserListAdapter;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;

import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

/**
 * Created by NurlanSofiyev on 22.09.2017.
 */

public class UserListHandler extends AsyncHttpResponseHandler {

    private List<UserModel> list;
    private ListView listView;
    private Context context;
    private Activity activity;
    private MultiStateView multiStateView;
    private SessionManager sessionManager;
    private TryAgainListener listener;
    private UserListAdapter adapter;


    public static void loadUserList(Context context, Activity activity, List<UserModel> list, ListView listView,
                                    TryAgainListener listener, MultiStateView multiStateView,
                                    UserListAdapter adapter){

        UserListHandler handler = new UserListHandler();
        handler.list = list;
        handler.adapter = adapter;
        handler.multiStateView = multiStateView;
        handler.listener = listener;
        handler.listView = listView;
        handler.context = context;
        handler.activity = activity;

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;

        SessionManager sessionManager =  SessionManager.getInstance(context);
        handler.sessionManager = sessionManager;
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));
            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");


        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        client.post(context, Constants.get_Users , entity, "text/plain", handler);

    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));
            if( jsonObject.getBoolean("success") ){
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                list.clear();
                for(int i = 0 ; i < jsonArray.length(); ++i){
                    JSONObject object = jsonArray.getJSONObject(i);
                    JSONObject user = object.getJSONObject("user");
                    UserModel model = new UserModel();
                    model.setUsername( user.getString("username") );
                    model.setFirstName( user.getString("firstname") );
                    model.setLastName( user.getString("lastname") );
                    model.setId( object.getInt("messageThreadId") );

                    if( !model.getUsername().trim().equals( sessionManager.getUserName().trim() ) ) {
                        list.add(model);
                    }
                }
                adapter.notifyDataSetChanged();
                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(activity, Chat.class);

                        intent.putExtra("object", list.get(position));
                        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(intent);

                    }
                });



            }else{
                multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            }
        }catch (JSONException e){
            e.printStackTrace();
            multiStateView.setViewState(MultiStateView.ViewState.ERROR);
            multiStateView.getView(MultiStateView.ViewState.ERROR).findViewById(R.id.retry)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.tryAgain();
                            //MessageListHandler.loadMessageList(context,list,listView,adapter,multiStateView);
                        }
                    });
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        multiStateView.setViewState(MultiStateView.ViewState.ERROR);
        multiStateView.getView(MultiStateView.ViewState.ERROR).findViewById(R.id.retry)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.tryAgain();
                        //MessageListHandler.loadMessageList(context,list,listView,adapter,multiStateView);
                    }
                });
    }

}
