package sofiyev.nurlan.pronetchat.handler;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.BaseAdapter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.CircleImageView;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

public class GetPhotoHandler extends AsyncHttpResponseHandler {

    private Context context;
    private Activity activity;
    private CircleImageView circleImageView;



    public static void load(Context context, Activity activity, CircleImageView circleImageView){


        GetPhotoHandler handler = new GetPhotoHandler();
        handler.context = context;
        handler.activity = activity;
        handler.circleImageView = circleImageView;
        AsyncHttpClient client = new AsyncHttpClient();

        SessionManager sessionManager = SessionManager.getInstance(context);

        client.get(Constants.get_Photo + sessionManager.getKeyToken() + "&thumb=true",handler );

            //    getProfilePicture();
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            Bitmap bm = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            circleImageView.setImageBitmap(bm);
            //circleImageView.getDrawable().getbi
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
    }
}
