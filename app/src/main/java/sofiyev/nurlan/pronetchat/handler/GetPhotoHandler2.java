package sofiyev.nurlan.pronetchat.handler;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.BaseAdapter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.CircleImageView;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

public class GetPhotoHandler2 extends AsyncHttpResponseHandler {

    private Context context;
    private Activity activity;
    private CircleImageView circleImageView;
    private BaseAdapter baseAdapter;
    private String username;

    private static HashMap<String, Bitmap> ramModel = new HashMap<>();

    public static Bitmap getMap(String username){
        return ramModel.get(username);
    }

    public static void putBitMap(String username,Bitmap bitmap){
        ramModel.put(username,bitmap);
    }

    public static void load(Context context, Activity activity, CircleImageView circleImageView,
                            BaseAdapter baseAdapter, String username){
        username = username.trim();
        if( ramModel.containsKey(username) ){
            Bitmap temp = getMap(username);
            if( temp != null ){
                circleImageView.setImageBitmap(temp);
            }
            return;
        }


        GetPhotoHandler2 handler = new GetPhotoHandler2();
        handler.context = context;
        handler.username = username;
        handler.activity = activity;
        handler.baseAdapter = baseAdapter;
        handler.circleImageView = circleImageView;
        AsyncHttpClient client = new AsyncHttpClient();

        SessionManager sessionManager = SessionManager.getInstance(context);

        client.get(Constants.get_Photo2 + sessionManager.getKeyToken()
                + "&username=" + username.trim() + "&thumb=true",handler );
    }



    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            Bitmap bm = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            circleImageView.setImageBitmap(bm);
            if( baseAdapter != null ){
                baseAdapter.notifyDataSetChanged();
            }
            putBitMap( username, bm );

            //circleImageView.getDrawable().getbi
        } catch (Exception e) {
            e.printStackTrace();
            putBitMap(username,null);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        putBitMap(username,null);
    }
}
