package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.listener.GetPhotoListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

public class UploadPhotoHandler extends AsyncHttpResponseHandler {

    private GetPhotoListener listener;

    public static void load(Context context, String path, GetPhotoListener listener){


        UploadPhotoHandler handler = new UploadPhotoHandler();
        handler.listener = listener;

        File file = new File(path);

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(120000);
        RequestParams params = new RequestParams();
        try {
            params.put("file", new File(path));
        } catch (FileNotFoundException ignored) {
            ignored.printStackTrace();
        }
        SessionManager sessionManager = SessionManager.getInstance(context);
        String url = Constants.upload_Photo + sessionManager.getKeyToken() ;
        client.post(url, params, handler );

            //    getProfilePicture();






    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        listener.loadPhoto();
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
    }
}
