package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.EmojiMapUtil;
import sofiyev.nurlan.pronetchat.adapter.ChatAdapter;
import sofiyev.nurlan.pronetchat.listener.NewMessagesListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.models.MessageModel;
import sofiyev.nurlan.pronetchat.models.StatusType;
import sofiyev.nurlan.pronetchat.models.UserModel;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class CreateNewThread extends AsyncHttpResponseHandler {

    private List<MessageModel> messages;
    private ListView listView;
    private ChatAdapter chatAdapter;
    private int position;
    private NewMessagesListener listener;
    private UserModel userModel;

    public static void send(Context context, List<MessageModel> messages, ListView listView, ChatAdapter chatAdapter,
                            NewMessagesListener listener, UserModel userModel,
                            String message, final int i){

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;

        CreateNewThread createNewThread = new CreateNewThread();
        createNewThread.messages = messages;
        createNewThread.listView = listView;
        createNewThread.chatAdapter = chatAdapter;
        createNewThread.position = i;
        createNewThread.listener = listener;
        createNewThread.userModel = userModel;

        SessionManager sessionManager = SessionManager.getInstance(context);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));

            nameValuePairs.add(new BasicNameValuePair("messageContent", EmojiMapUtil.replaceUnicodeEmojis(message)  ));
            nameValuePairs.add(new BasicNameValuePair("to", userModel.getUsername() ) );

            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        client.post(context, Constants.create_NewThread, entity, "text/plain", createNewThread );


    }


    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));


            if (jsonObject.getBoolean("success")) {

                messages.get(position - 1).setStatus(StatusType.SEND);
                userModel.setId(jsonObject.getInt("messageThreadId"));
                //chatAdapter.notifyDataSetChanged();
                //listener.getNewMessages(true, position);

            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
    }


}
