package sofiyev.nurlan.pronetchat.handler;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.Util;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;
import sofiyev.nurlan.pronetchat.widget.MyEditText;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

public class EditProfileHandler extends AsyncHttpResponseHandler {



    private Context context;
    private TryAgainListener listener;


    public static void load(Context context, MyEditText p_email, MyEditText p_lastname, MyEditText p_surname,
                            MyEditText p_username, MultiStateView multiStateView, TryAgainListener listener
                           ){
        EditProfileHandler handler = new EditProfileHandler();
        handler.listener = listener;
        handler.context = context;

        SessionManager sessionManager = SessionManager.getInstance(context);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));

            nameValuePairs.add(new BasicNameValuePair("firstname", p_surname.getText().toString().trim() ) );
            nameValuePairs.add(new BasicNameValuePair("lastname", p_lastname.getText().toString().trim() ) );
            nameValuePairs.add(new BasicNameValuePair("email", p_email.getText().toString().trim() ) );

            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));

            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

        } catch (Exception ignored) {
            ignored.printStackTrace();

        }

        client.post(context, Constants.update_Account_Details, entity, "text/plain", handler );

    }


    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        Log.e("responseNew5", new String(responseBody));
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));


            if (jsonObject.getBoolean("success")) {
                Toast.makeText(context,"Yeniləndi", Toast.LENGTH_LONG );
                listener.tryAgain();
            } else {
                Toast.makeText(context, "Xəta oldu", Toast.LENGTH_LONG );
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, "Xəta oldu", Toast.LENGTH_LONG );
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        Toast.makeText(context, "Xəta oldu", Toast.LENGTH_LONG );
    }
}

