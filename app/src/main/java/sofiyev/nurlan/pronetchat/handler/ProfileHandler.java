package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.EmojiMapUtil;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.models.StatusType;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;
import sofiyev.nurlan.pronetchat.widget.MyEditText;

/**
 * Created by NurlanSofiyev on 24.09.2017.
 */

public class ProfileHandler extends AsyncHttpResponseHandler {

    private MyEditText p_email;
    private MyEditText p_username;
    private MyEditText p_surname;
    private MyEditText p_lastname;
    private MultiStateView multiStateView;
    private TryAgainListener listener;




    public static void load(Context context, MyEditText p_email, MyEditText p_lastname, MyEditText p_surname,
                            MyEditText p_username, MultiStateView multiStateView,
                            TryAgainListener listener){
        ProfileHandler handler = new ProfileHandler();
        handler.p_email = p_email;
        handler.p_lastname = p_lastname;
        handler.p_surname = p_surname;
        handler.p_username = p_username;
        handler.multiStateView = multiStateView;
        handler.listener = listener;
        SessionManager sessionManager = SessionManager.getInstance(context);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));

            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

        } catch (Exception ignored) {
            ignored.printStackTrace();

        }

        client.post(context, Constants.get_Account_Details, entity, "text/plain", handler );

    }


    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));


            if (jsonObject.getBoolean("success")) {
                JSONObject user = jsonObject.getJSONObject("user");
                String username = user.getString("username");
                String firstname = user.getString("firstname");
                String lastname = user.getString("lastname");
                String email = user.getString("email");
                p_username.setText(username);
                p_surname.setText(firstname);
                p_lastname.setText(lastname);
                p_email.setText(email);

            } else {

            }
            multiStateView.setViewState(MultiStateView.ViewState.CONTENT);

        } catch (JSONException e) {
            e.printStackTrace();
            multiStateView.setViewState(MultiStateView.ViewState.ERROR);
            multiStateView.getView(MultiStateView.ViewState.ERROR).findViewById(R.id.retry)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.tryAgain();
                            //MessageListHandler.loadMessageList(context,list,listView,adapter,multiStateView);
                        }
                    });
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        multiStateView.setViewState(MultiStateView.ViewState.ERROR);
        multiStateView.getView(MultiStateView.ViewState.ERROR).findViewById(R.id.retry)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.tryAgain();
                        //MessageListHandler.loadMessageList(context,list,listView,adapter,multiStateView);
                    }
                });

    }
}
