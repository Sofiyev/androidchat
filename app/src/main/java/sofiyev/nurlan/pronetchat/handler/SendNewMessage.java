package sofiyev.nurlan.pronetchat.handler;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.EmojiMapUtil;
import sofiyev.nurlan.pronetchat.activity.Chat;
import sofiyev.nurlan.pronetchat.adapter.ChatAdapter;
import sofiyev.nurlan.pronetchat.listener.NewMessagesListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.models.MessageModel;
import sofiyev.nurlan.pronetchat.models.StatusType;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class SendNewMessage extends AsyncHttpResponseHandler {


    private List<MessageModel> messages;
    private ListView listView;
    private ChatAdapter chatAdapter;
    private int position;
    private NewMessagesListener listener;
    private Chat chat;

    public static void send(Context context, List<MessageModel> messages, ListView listView, ChatAdapter chatAdapter,
                            int id, String message, final int i, NewMessagesListener newMessagesListener,
                            Chat chat){

        SendNewMessage sendNewMessage = new SendNewMessage();
        sendNewMessage.messages = messages;
        sendNewMessage.listView = listView;
        sendNewMessage.chat = chat;
        sendNewMessage.chatAdapter = chatAdapter;
        sendNewMessage.position = i;
        sendNewMessage.listener = newMessagesListener;


        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;

        SessionManager sessionManager = SessionManager.getInstance(context);

        try {

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));
            nameValuePairs.add(new BasicNameValuePair("messageContent", EmojiMapUtil.replaceUnicodeEmojis(message)  ));
            nameValuePairs.add(new BasicNameValuePair("messageThreadId", id + ""));

            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        client.post(context, Constants.send_NewMessage, entity, "text/plain", sendNewMessage) ;

    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));

            if (jsonObject.getBoolean("success")) {

                messages.get(position - 1).setStatus(StatusType.SEND);
                chatAdapter.notifyDataSetChanged();
                listener.getNewMessages(true, position,chat);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

    }
}
