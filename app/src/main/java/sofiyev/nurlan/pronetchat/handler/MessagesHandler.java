package sofiyev.nurlan.pronetchat.handler;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import sofiyev.nurlan.pronetchat.Utils.Constants;
import sofiyev.nurlan.pronetchat.Utils.EmojiMapUtil;
import sofiyev.nurlan.pronetchat.adapter.ChatAdapter;
import sofiyev.nurlan.pronetchat.listener.RepeatListener;
import sofiyev.nurlan.pronetchat.manager.SessionManager;
import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.models.MessageModel;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.models.UserType;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class MessagesHandler extends AsyncHttpResponseHandler {

    private Context context;
    private List<MessageModel> messages;
    private ListView listView;
    private ChatAdapter chatAdapter;
    private AtomicInteger lastId;
    private Activity activity;
    private RepeatListener repeat;


    public static void loadMessages(Activity activity, Context context, List<MessageModel> messages,
                                    ListView listView, AtomicInteger lastId, ChatAdapter chatAdapter,
                                    RepeatListener repeat, UserModel userModel){
        MessagesHandler handler = new MessagesHandler();
        handler.activity = activity;
        handler.context = context;
        handler.messages = messages;
        handler.listView = listView;
        handler.lastId = lastId;
        handler.chatAdapter = chatAdapter;
        handler.repeat = repeat;
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        StringEntity entity = null;
        SessionManager sessionManager = SessionManager.getInstance(context);

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", sessionManager.getKeyToken()));
            nameValuePairs.add(new BasicNameValuePair("start", ""));
            nameValuePairs.add(new BasicNameValuePair("limit", ""));
            nameValuePairs.add(new BasicNameValuePair("messageThreadId", userModel.getId() + ""));

            Log.e("params", nameValuePairs.toString());
            entity = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
            Log.e("entitiy", entity + "");


        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        client.post(context, Constants.get_MessageOfCurrentThread, entity, "text/plain", handler );


    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        Log.e("Response", new String(responseBody));
        try {
            JSONObject jsonObject = new JSONObject(new String(responseBody));
            messages.clear();
            if (jsonObject.getBoolean("success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = jsonArray.length() - 1; i >= 0; i--) {

                    JSONObject object = jsonArray.getJSONObject(i);
                    String globalName = jsonObject.getString("user");

                    MessageModel message = new MessageModel();
                    message.setMessage( EmojiMapUtil.replaceCheatSheetEmojis( object.getString("text")  )  );

                    message.setId(object.getInt("id"));
                    if (jsonObject.getString("user").toLowerCase().equals(object.getJSONObject("user").getString("username").toLowerCase()))
                        message.setUserType(UserType.USER);
                    else
                        message.setUserType(UserType.USER_FROM);

                    //lastId = object.getInt("id");
                    lastId.set( object.getInt("id") );

                    messages.add(message);
                }


            } else {

            }

            chatAdapter.notifyDataSetChanged();
            repeat.startRepeat();

            listView.post(new Runnable() {
                public void run() {
                    listView.setSelection(listView.getCount() - 1);
                }
            });

        } catch (JSONException e) {
            //System.out.println("bashladi !!!!!!!!!!\n\n\n\n\n\n\n\n\\n\n\n\n\n\n");
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

    }

}
