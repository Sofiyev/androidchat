package sofiyev.nurlan.pronetchat.Utils;

import android.app.Activity;
import android.content.Context;

/**
 * Created by NurlanSofiyev on 21.09.2017.
 */

public class Constants {

    public static final String root_URL = "http://192.168.1.68:8080/";
    public static final String login_URL = root_URL + "user/login";
    public static final String register_URL = root_URL + "user/signUp";
    public static final String last_MessageOfThreads = root_URL + "api/getLastMessageOfThreads";
    public static final String get_Users = root_URL + "api/getUsers";
    public static final String get_MessageOfCurrentThread = root_URL + "api/getMessagesOfThread";
    public static final String send_NewMessage = root_URL + "api/sendMessage";
    public static final String create_NewThread = root_URL + "api/createMessage";
    public static final String get_NewMessage = root_URL + "api/getNewMessages";
    public static final String get_Account_Details = root_URL + "api/getAccountDetails";
    public static final String update_Account_Details = root_URL + "api/updateAccountDetails";
    public static final String upload_Photo = root_URL + "api/uploadPhoto?token=";
    public static final String get_Photo = root_URL + "api/getProfilePicture?token=";
    public static final String get_Photo2 = root_URL + "api/getProfilePicture2?token=";
    public static final int INTERVAL = 1000 * 4;
    public static Context context;
    public static Activity activity;

    public static final int CAMERA_REQUEST = 1888;

}
