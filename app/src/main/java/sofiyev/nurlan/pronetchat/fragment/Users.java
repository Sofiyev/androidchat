package sofiyev.nurlan.pronetchat.fragment;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.adapter.UserListAdapter;
import sofiyev.nurlan.pronetchat.handler.UserListHandler;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.models.UserModel;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

import static sofiyev.nurlan.pronetchat.Utils.Constants.INTERVAL;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class Users extends Fragment implements TryAgainListener {

    private ListView listView;
    private List<UserModel> list;

    private MultiStateView multiStateView;
    private UserListAdapter adapter;

    public Users(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(runnable, INTERVAL);
            getUsers();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_users, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        multiStateView = (MultiStateView) view.findViewById(R.id.multiStateView);
        list = new ArrayList<>();
        adapter = new UserListAdapter(getActivity(),list);
        listView.setAdapter(adapter);
        getUsers();
        handler.post(runnable);
        return view;
    }

    public void getUsers(){
        UserListHandler.loadUserList(getContext(),getActivity(),list,listView,this,multiStateView,adapter);

    }

    @Override
    public void tryAgain() {
        multiStateView.setViewState(MultiStateView.ViewState.LOADING);
        getUsers();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if( handler != null ){
            handler.removeCallbacks(runnable);
        }
    }
}
