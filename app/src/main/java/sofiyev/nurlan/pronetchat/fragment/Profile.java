package sofiyev.nurlan.pronetchat.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.handler.EditProfileHandler;
import sofiyev.nurlan.pronetchat.handler.GetPhotoHandler;
import sofiyev.nurlan.pronetchat.handler.ProfileHandler;
import sofiyev.nurlan.pronetchat.handler.UploadPhotoHandler;
import sofiyev.nurlan.pronetchat.listener.GetPhotoListener;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.widget.CircleImageView;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;
import sofiyev.nurlan.pronetchat.widget.MyEditText;

import static android.app.Activity.RESULT_OK;
import static sofiyev.nurlan.pronetchat.Utils.Util.getImageUri;
import static sofiyev.nurlan.pronetchat.Utils.Util.getRealPathFromURI;
import static sofiyev.nurlan.pronetchat.Utils.Util.hasPermissions;
import static sofiyev.nurlan.pronetchat.Utils.Util.requestCameraPermission;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class Profile extends Fragment implements TryAgainListener, GetPhotoListener {

    private MyEditText p_username;
    private MyEditText p_firstname;
    private MyEditText p_lastname;
    private MyEditText p_email;
    private MultiStateView multiStateView;
    private CircleImageView imageView;
    private Button button;
    private Button uploadButton;
    private String filePath;
    public Profile(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        p_username = (MyEditText) view.findViewById(R.id.p_username);
        p_firstname = (MyEditText) view.findViewById(R.id.p_firstname);
        p_lastname = (MyEditText) view.findViewById(R.id.p_lastname);
        p_email = (MyEditText) view.findViewById(R.id.p_email);
        button = (Button) view.findViewById(R.id.button);
        imageView = (CircleImageView) view.findViewById(R.id.imageView);
        uploadButton = (Button) view.findViewById(R.id.uploadButton);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        multiStateView = (MultiStateView) view.findViewById(R.id.multiStateView);
        loadInfo();
        loadPhoto();
        return view;
    }

    private void upload() {
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        } else {
            selectImage();
        }

    }

    private void save() {
        EditProfileHandler.load(getActivity(),p_email,p_lastname,p_firstname,p_username,multiStateView,this);
    }

    private void loadInfo(){
        multiStateView.setViewState(MultiStateView.ViewState.LOADING);
        ProfileHandler.load(getActivity(),p_email,p_lastname,p_firstname,p_username,multiStateView,this);
    }

    @Override
    public void tryAgain() {
        loadInfo();
    }

    public void showCamera() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            requestCameraPermission(getActivity());

        } else {

            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePicture, 0);
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Şəkil çək", "Yaddaşdan götür",
                "İmtina"};

        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());


        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals( items[0] )) {

                    showCamera();

                } else if (items[item].equals(items[1])) {

                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);
                } else if (items[item].equals(items[2])) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


     public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {

                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");

                        Uri tempUri = getImageUri(getActivity(), photo);

                        File finalFile = new File(getRealPathFromURI(tempUri, getActivity() ));
                        Log.e("filePathFinal", finalFile.getPath() + "");
                        filePath = finalFile.getPath() + "";
                        uploadPhotoMessage(filePath);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    try {

                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        Cursor cursor = getActivity().getContentResolver().query(
                                selectedImage, filePathColumn, null, null, null);
                        assert cursor != null;
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String stringPath = cursor.getString(columnIndex);
                        cursor.close();
                        filePath = stringPath;

                        Log.e("filePathFinal", filePath + "");
                        uploadPhotoMessage(filePath);


                    } catch (Exception e) {
                        try {
                            Log.e("Actionm", e + "1");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                break;
        }

    }

    private void uploadPhotoMessage(String filePath) {

        UploadPhotoHandler.load(getActivity(),filePath,this);
    }


    @Override
    public void loadPhoto() {
        GetPhotoHandler.load(getContext(),getActivity(),imageView);
    }
}
