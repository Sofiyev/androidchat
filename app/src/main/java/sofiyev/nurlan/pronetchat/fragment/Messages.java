package sofiyev.nurlan.pronetchat.fragment;


import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import sofiyev.nurlan.pronetchat.R;
import sofiyev.nurlan.pronetchat.adapter.MessageListAdapter;
import sofiyev.nurlan.pronetchat.handler.MessageListHandler;
import sofiyev.nurlan.pronetchat.listener.TryAgainListener;
import sofiyev.nurlan.pronetchat.models.ChatModel;
import sofiyev.nurlan.pronetchat.widget.MultiStateView;

import static sofiyev.nurlan.pronetchat.Utils.Constants.INTERVAL;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class Messages extends Fragment implements TryAgainListener {

    private ListView listView;
    private List<ChatModel> list;
    private MessageListAdapter adapter;
    private Handler handler = new Handler();

    private MultiStateView stateView;

    public Messages(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(runnable, INTERVAL);
            getMessages();
        }
    };



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_messages, container, false);
        stateView = (MultiStateView) view.findViewById(R.id.multiStateView);
        listView = (ListView) view.findViewById(R.id.listView);
        list = new ArrayList<>();
        adapter = new MessageListAdapter(getContext(), list);
        listView.setAdapter(adapter);
        getMessages();

        handler.post(runnable);
        return view;
    }

    private void getMessages() {
        MessageListHandler.loadMessageList( getContext(), list,listView,adapter,stateView,this);
    }


    @Override
    public void tryAgain() {
        stateView.setViewState(MultiStateView.ViewState.LOADING);
        getMessages();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(handler != null){
            handler.removeCallbacks(runnable);

        }
    }

}
