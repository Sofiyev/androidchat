package sofiyev.nurlan.pronetchat.manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by NurlanSofiyev on 23.09.2017.
 */

public class SessionManager {

    public static boolean close = false;

    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    public static SessionManager sessionManager;

    private static final String PREF_NAME = "pronetchat";

    private static final String IS_LOGIN = "isLogged";


    public static final String KEY_TOKEN = "token";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_FIRSTNAME = "firstname";
    public static final String KEY_LASTNAME = "lastname";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_PREFIX = "token";
    public static final String KEY_NEXTSCREEN="nextScreen";
    public static final String KEY_LOGIN = "login";

    public static final String KEY_PASSWORD = "password";

    public static final String KEY_LANG = "lang";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    public static SessionManager getInstance(Context context){
        if( sessionManager == null ){
            sessionManager = new SessionManager(context);
        }
        return sessionManager;
    }


    public void phonePrefix(String prefix) {
        editor.putString(KEY_PREFIX, prefix);
        editor.apply();
    }
    public void fullName(String name) {
        editor.putString(KEY_FIRSTNAME, name);
        editor.apply();
    }


    public void createLoginSession(String phone, String username, String lastname, String firstname, String password, String email) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PHONE, phone);
        editor.putString(KEY_LASTNAME, lastname);
        editor.putString(KEY_FIRSTNAME, firstname);
        editor.putString(KEY_PASSWORD, password);
        editor.apply();
    }

    public void signUp(String username, String email) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_EMAIL, email);
        editor.apply();
    }

    public void passIntro(boolean alfa){
        editor.putBoolean(KEY_NEXTSCREEN,alfa);
        editor.apply();
    }

    public void login(String token, String username, String password) {
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_PASSWORD, password);
        editor.apply();
    }

    public void logOut() {
        editor.clear();
        editor.apply();
    }

    public boolean logedIn() {
        boolean a;
        if (pref.contains(KEY_TOKEN)) a = true;
        else a = false;

        return a;
    }

    public boolean nextScreen(){
        boolean b;
        b = pref.contains(KEY_NEXTSCREEN);

        return b;
    }

    public String getUserName(){return pref.getString(KEY_USERNAME,"");}

    public String getKeyUsername() {
        return pref.getString(KEY_FIRSTNAME, "");
    }

    public String getKeyToken() {
        return pref.getString(KEY_TOKEN, "");
    }
}
